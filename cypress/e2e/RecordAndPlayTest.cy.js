describe('Record and Play Suite', () => {
  it('Test1', () => {
    /* ==== Generated with Cypress Studio ==== */
    cy.visit('https://www.youtube.com/');
    cy.get('.ytd-topbar-menu-button-renderer > .style-scope > .yt-spec-icon-shape > div').click();
    cy.get(':nth-child(3) > #endpoint > tp-yt-paper-item.style-scope > #right-icon > .style-scope > .yt-spec-icon-shape > div').click();
    cy.get(':nth-child(12) > #endpoint > tp-yt-paper-item.style-scope').click();
    cy.get('#search-input > #search').clear('t');
    cy.get('#search-input > #search').type('testers talk');
    cy.get('#search-icon-legacy > yt-icon.style-scope > .style-scope > .yt-spec-icon-shape > div').click();
    cy.get('#channel-title > #container > #text-container > #text').click();
    cy.get('#c4-player > .ytp-chrome-bottom > .ytp-chrome-controls > .ytp-left-controls > .ytp-play-button').click();
    cy.get(':nth-child(3) > #details > h3.style-scope > #video-title').click();
    cy.get('#movie_player > .ytp-chrome-bottom > .ytp-chrome-controls > .ytp-left-controls > .ytp-prev-button').click();
    cy.get('#movie_player > .ytp-chrome-bottom > .ytp-chrome-controls > .ytp-left-controls > .ytp-play-button').click();
    /* ==== End Cypress Studio ==== */
  })

  /* ==== Test Created with Cypress Studio ==== */
  it('SearchJavaScriptByTestersTalkTest', function() {
    /* ==== Generated with Cypress Studio ==== */
    cy.visit('https://www.youtube.com/');
    cy.get('.ytd-topbar-menu-button-renderer > .style-scope > .yt-spec-icon-shape > div').click();
    cy.get(':nth-child(3) > #endpoint > tp-yt-paper-item.style-scope > #right-icon > .style-scope > .yt-spec-icon-shape > div').click();
    cy.get(':nth-child(12) > #endpoint > tp-yt-paper-item.style-scope').click();
    cy.get('#search-input > #search').clear('j');
    cy.get('#search-input > #search').type('javascript by testers talk');
    cy.get('#search-icon-legacy > yt-icon.style-scope > .style-scope > .yt-spec-icon-shape > div').click();
    /* ==== End Cypress Studio ==== */
  });
})