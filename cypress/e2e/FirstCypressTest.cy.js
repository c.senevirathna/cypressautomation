describe('Test Suite 1', () => {
  it('My First Test', () => {
    cy.log("Test1 started.....");
    cy.visit('https://www.youtube.com/');
    cy.log("Navigated to youtube.");
    cy.get('input[id="search"]').type("javaScript by testers talk");
    cy.get('button[id="search-icon-legacy"]>yt-icon').click();
    cy.log("Test1 Completed....         .");
  })
})